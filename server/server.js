
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var port = 8080;
var path = require('path');
var io = require('socket.io')(server)

var cookieParser = require('cookie-parser');
var mongodb = require('mongodb').MongoClient;
var bodyParser = require('body-parser');

var configDB = require('../config/database.js');

app.use(express.static(path.join(__dirname, "../client")));

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.set('view engine', 'ejs');

io.on("connection",function(socket){

      setInterval(()=>{
        mongodb.connect(configDB.url, function(err, db) {
        var col = db.collection('fire');

        col.find({}).toArray(function(err, items) {
          console.log(items);
          socket.emit('fire !', { fire: items[0]});
          db.close();
        });
        });
      }, 1000);
});

server.listen(port, "0.0.0.0");

app.route('/').get(function(req, res) {

  mongodb.connect(configDB.url, function(err, db) {

    var col = db.collection('beacon');

    col.find({}).toArray(function(err, items) {
      db.close();
      res.render(
      "pages/index",
      {
        beacons_data: items
      });
    });
  });
});


app.get('*', function(req, res) {
    res.redirect('/');
});

console.log('Server running on port: ' + port);
