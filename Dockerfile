FROM node:9.1-alpine

LABEL authors="Mathieu Tortuyaux <mathieu.tortuyaux@gmail.com>"

RUN npm install -g nodemon


WORKDIR /opt/webserver/
COPY . /opt/webserver
RUN npm install

CMD  ["npm","run","start"]
EXPOSE 8080

RUN rm -rf /tmp/* /var/tmp/*
