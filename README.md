### Middleware WebApp sub-project for SoundTrack project

##### Getting started

clone this repo and go into:

```shell
$ git clone git@gitlab.com:soundtrack/webapp.git
$ cd webapp
```

Please make sure that [Docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/) are installed on your machine

```shell
$ docker version
Client:
 Version:      17.09.0-ce
 API version:  1.32
 Go version:   go1.8.3
 Git commit:   afdb6d4
 Built:        Tue Sep 26 22:40:09 2017
 OS/Arch:      windows/amd64

Server:
 Version:      17.09.0-ce
 API version:  1.32 (minimum version 1.12)
 Go version:   go1.8.3
 Git commit:   afdb6d4
 Built:        Tue Sep 26 22:45:38 2017
 OS/Arch:      linux/amd64
 Experimental: true
```

You should have something like that.

You can build your image locally, if you are in development mode. Or you can pull it directly from `Gitlab` registry.

```shell
# build locally the image
$ docker build -t webapp:test-0.1 .
# or pull latest image it from registry
$ docker login registry.gitlab.com
# Download latest or a specific tag
$ docker pull registry.gitlab.com/soundtrack/webapp:latest
```

We are ready to run our `wepapp` container:

```shell
$ docker run --name webapp-container -p "8080:8080" webapp
$ docker ps
# If you want to stop it
$ docker stop webapp-container
# Or simply with Docker Compose
$ docker-compose up -d
```
Then, go on [local](http:localhost:8080).

`docker-compose` will pull `mongodb(3.4.10)` image for you. :smiley:

This a `dev` repo, so `node server` is actually `nodemon` which one it's a live reload server. So you can write your code, save it, and node will automatically restart your server.
If you want to have those benefits with `docker`, you need to use `docker-compose-development.yaml`, it will build your image locally and share your OS volume with container volume. (If you are on windows you will have to enter your credentials)

```shell
$ docker-compose -f docker-compose-development.yaml up
```

##### Contributing

Fork this repo, then create a branch with your feature and perfom a MR.
```shell
$ git checkout -b my-awesome-feature master
$ git commit -m "feat(X): do that"
# Be sure that origin is your forked repo
$ git push -u origin my-awesome-feature
```

Do not forget to write test. Once your MR approved, you can squash your commit.

##### Conclusion

This image is useless alone. You have to use it in a `docker` ecosystem with a database and a Web server.
